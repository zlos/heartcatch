#pragma once
#ifndef _HEARTCATCH_JOB_MANAGER_IMPL_H_
#define _HEARTCATCH_JOB_MANAGER_IMPL_H_

#include <Windows.h>
#include <vector>
#include <mutex>

namespace Heartcatch
{

    class Job;
    class JobHandle;

    namespace Impl
    {

        class JobManagerImpl final
        {
        public:
            JobManagerImpl();
            ~JobManagerImpl();
            bool Init();
            void Shutdown();
            void Run(JobHandle *handle, Job **jobs, size_t jobCount);
        private:
            Job *FetchNextJob();
            static DWORD CALLBACK ThreadProc(void *arg);
            void ClearJobQueue();
            void DestroyJobThreads();
            void QueueJobs(JobHandle *handle, Job **jobs, size_t jobCount);
        private:
            std::vector<HANDLE> threadPool;
            std::vector<Job*>   jobQueue;
            std::mutex          jobQueueMutex;
            HANDLE              jobIsAvailableSignal;
            HANDLE              shutDownSignal;
        };

    }
}

#endif // _HEARTCATCH_JOB_MANAGER_IMPL_H_
