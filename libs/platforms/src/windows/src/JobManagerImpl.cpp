#include "JobManagerImpl.h"
#include "heartcatch/Job.h"
#include "heartcatch/JobHandle.h"
#include <cassert>

using namespace Heartcatch;
using namespace Heartcatch::Impl;

JobManagerImpl::JobManagerImpl()
    : jobIsAvailableSignal(INVALID_HANDLE_VALUE)
    , shutDownSignal(INVALID_HANDLE_VALUE)
{}

JobManagerImpl::~JobManagerImpl()
{}

bool JobManagerImpl::Init()
{
    shutDownSignal = CreateEventW(nullptr, true, false, nullptr);
    jobIsAvailableSignal = CreateEventW(nullptr, false, false, nullptr);
    SYSTEM_INFO sysInfo;
    GetNativeSystemInfo(&sysInfo);
    DWORD threadCount = sysInfo.dwNumberOfProcessors * 2;
    threadPool.resize(threadCount);
    for (DWORD i = 0; i < threadCount; ++i)
    {
        threadPool[i] = CreateThread(nullptr, 0, JobManagerImpl::ThreadProc, this, 0, nullptr);
    }
    return true;
}

void JobManagerImpl::ClearJobQueue()
{
    std::lock_guard<std::mutex> guard(jobQueueMutex);
    jobQueue.clear();
}

void JobManagerImpl::DestroyJobThreads()
{
    std::lock_guard<std::mutex> guard(jobQueueMutex);
    for (unsigned i = 0; i < threadPool.size(); ++i)
    {
        CloseHandle(threadPool[i]);
        threadPool[i] = INVALID_HANDLE_VALUE;
    }
    threadPool.clear();
}

void JobManagerImpl::Shutdown()
{
    ClearJobQueue();
    SetEvent(shutDownSignal);
    WaitForMultipleObjects(threadPool.size(), threadPool.data(), true, INFINITE);
    DestroyJobThreads();
    CloseHandle(shutDownSignal);
    CloseHandle(jobIsAvailableSignal);
    shutDownSignal = INVALID_HANDLE_VALUE;
    jobIsAvailableSignal = INVALID_HANDLE_VALUE;
}

void JobManagerImpl::QueueJobs(JobHandle *handle, Job **jobs, size_t jobCount)
{
    std::lock_guard<std::mutex> guard(jobQueueMutex);
    for (size_t i = 0; i < jobCount; ++i)
    {
        Job *job = jobs[i];
        assert(job != nullptr);
        jobQueue.push_back(job);
        job->OnQueued(handle);
    }
}

void JobManagerImpl::Run(JobHandle *handle, Job **jobs, size_t jobCount)
{
    assert(handle != nullptr);
    assert(jobs != nullptr);
    assert(jobCount > 0);
    QueueJobs(handle, jobs, jobCount);
    SetEvent(jobIsAvailableSignal);
}

DWORD JobManagerImpl::ThreadProc(void *params)
{
    JobManagerImpl *jobManager = static_cast<JobManagerImpl*>(params);
    HANDLE events[2] = { jobManager->shutDownSignal, jobManager->jobIsAvailableSignal };
    while (true)
    {
        DWORD eventNum = WaitForMultipleObjects(_countof(events), events, false, INFINITE);
        if (eventNum == WAIT_OBJECT_0)
        {
            return 0;
        }
        while (Job *job = jobManager->FetchNextJob())
        {
            job->OnRun();
            job->OnFinished();
        }
    }
}

Job *JobManagerImpl::FetchNextJob()
{
    std::lock_guard<std::mutex> guard(jobQueueMutex);
    if (!jobQueue.empty())
    {
        Job *job = jobQueue[0];
        jobQueue.erase(begin(jobQueue));
        return job;
    }
    return nullptr;
}
