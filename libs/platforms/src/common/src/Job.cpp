#include "heartcatch/Job.h"
#include "heartcatch/JobHandle.h"
#include <cassert>

using namespace Heartcatch;

Job::Job()
    : handle(nullptr)
{}

Job::~Job()
{}

void Job::OnQueued(JobHandle *handle)
{
    assert(this->handle == nullptr);
    assert(handle != nullptr);
    this->handle = handle;
    handle->AddJob();
}

void Job::OnFinished()
{
    assert(handle != nullptr);
    handle->ReleaseJob();
}
