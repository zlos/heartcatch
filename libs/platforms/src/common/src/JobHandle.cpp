#include "heartcatch/JobHandle.h"
#include <thread>
#include <cassert>

using namespace Heartcatch;

JobHandle::JobHandle()
    : activeJobCount(0u)
{}

JobHandle::~JobHandle()
{}

bool JobHandle::IsFinished()const
{
    return activeJobCount == 0;
}

void JobHandle::Wait()
{
    while (!IsFinished())
    {
        std::this_thread::yield();
    }
}

void JobHandle::AddJob()
{
    activeJobCount++;
}

void JobHandle::ReleaseJob()
{
    assert(!IsFinished());
    activeJobCount--;
}
