#include <heartcatch/JobManager.h>
#include <heartcatch/Job.h>
#include <heartcatch/JobHandle.h>
#include <gtest/gtest.h>
#include <random>
#include <thread>

using namespace Heartcatch;

static const unsigned MANY_JOBS = 66;
static const unsigned SOME_JOBS = 11;

class WaitJob : public Job
{
public:
    WaitJob(unsigned ms)
        : ms(ms)
        , finished(false)
    {}
    bool OnRun()override
    {
        std::this_thread::sleep_for(std::chrono::duration<unsigned, std::milli>(ms));
        finished = true;
        return true;
    }
    bool IsFinished()const
    {
        return finished; 
    }
private:
    bool        finished;
    unsigned    ms;
};

TEST(JobManager, InitShutdown)
{
    JobManager jobManager;
    EXPECT_TRUE(jobManager.Init());
    EXPECT_NO_THROW(jobManager.Shutdown());
}

TEST(JobManager, SimpleJob)
{
    JobManager jobManager;
    EXPECT_TRUE(jobManager.Init());
    WaitJob job(10);
    JobHandle handle;
    jobManager.Run(&handle, &job);
    handle.Wait();
    EXPECT_TRUE(job.IsFinished());
    EXPECT_NO_THROW(jobManager.Shutdown());
}

TEST(JobManager, ManyJobs)
{
    JobManager jobManager;
    EXPECT_TRUE(jobManager.Init());
    WaitJob* jobs[MANY_JOBS];
    JobHandle handle;
    std::uniform_int_distribution<unsigned> unif(5, 15);
    std::default_random_engine re;
    for (unsigned i = 0; i < MANY_JOBS; ++i)
    {
        jobs[i] = new WaitJob(unif(re));
    }
    for (unsigned i = 0; i < MANY_JOBS; ++i)
    {
        jobManager.Run(&handle, jobs[i]);
    }
    handle.Wait();
    for (unsigned i = 0; i < MANY_JOBS; ++i)
    {
        EXPECT_TRUE(jobs[i]->IsFinished());
    }
    for (unsigned i = 0; i < MANY_JOBS; ++i)
    {
        delete jobs[i];
    }
    EXPECT_NO_THROW(jobManager.Shutdown());
}

TEST(JobManager, TwoJobGroupsParallel)
{
    JobManager jobManager;
    EXPECT_TRUE(jobManager.Init());
    WaitJob *jobs1[SOME_JOBS];
    WaitJob *jobs2[SOME_JOBS];
    JobHandle handle1;
    JobHandle handle2;
    std::uniform_int_distribution<unsigned> unif(5, 15);
    std::default_random_engine re;
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        jobs1[i] = new WaitJob(unif(re));
        jobs2[i] = new WaitJob(unif(re));
    }
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        jobManager.Run(&handle1, jobs1[i]);
        jobManager.Run(&handle1, jobs2[i]);
    }
    handle1.Wait();
    handle2.Wait();
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        EXPECT_TRUE(jobs1[i]->IsFinished());
        EXPECT_TRUE(jobs2[i]->IsFinished());
    }
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        delete jobs1[i];
        delete jobs2[i];
    }
    EXPECT_NO_THROW(jobManager.Shutdown());
}

TEST(JobManager, TwoJobGroupsSequence)
{
    JobManager jobManager;
    EXPECT_TRUE(jobManager.Init());
    WaitJob *jobs1[SOME_JOBS];
    WaitJob *jobs2[SOME_JOBS];
    JobHandle handle1;
    JobHandle handle2;
    std::uniform_int_distribution<unsigned> unif(5, 15);
    std::default_random_engine re;
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        jobs1[i] = new WaitJob(unif(re));
        jobs2[i] = new WaitJob(unif(re));
    }
    jobManager.Run(&handle1, (Job**)jobs1, _countof(jobs1));
    handle1.Wait();
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        EXPECT_TRUE(jobs1[i]->IsFinished());
    }
    jobManager.Run(&handle2, (Job**)jobs2, _countof(jobs2));
    handle2.Wait();
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        EXPECT_TRUE(jobs2[i]->IsFinished());
    }
    for (unsigned i = 0; i < SOME_JOBS; ++i)
    {
        delete jobs1[i];
        delete jobs2[i];
    }
    EXPECT_NO_THROW(jobManager.Shutdown());
}
